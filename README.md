# INIT
chmod 777 install.sh  
./install.sh  

Configurare Azure active directory
https://gitlab.com/coding-lab/prodigys/starter-projects/azure-active-directory

# CONFIGURE GIT
https://www.theserverside.com/blog/Coffee-Talk-Java-News-Stories-and-Opinions/How-to-configure-GitLab-SSH-keys-for-secure-Git-connections  

git config --global user.name "FIRST_NAME LAST_NAME"
git config --global user.email "MY_NAME@example.com"

# KPAX SETUP

cd /home/prodigys  
mkdir repo  
cd repo  

chmod 777 git-kpax.sh  
./git-kpax.sh  
  
# Autostart  
Extract autostart.zip into  
/.config/autostart  

# Security:
Installare lo script:
mettelo in una folder e recuperate il path con `pwd`.
Testate che vada con:
python3 /home/prodigys/repo/security/security.py
 
impostate il cron con
 
crontab -e
inserite nel file
30  7  *  *  2 python3 /home/prodigys/repo/security/security.py

https://gitlab.com/coding-lab/prodigys/system-administration/security

ATTENZIONE: per linux abilitare la crittografia del disco: https://jumpcloud.com/blog/how-to-enable-full-disk-encryption-ubuntu-22-04#:~:text=LUKS%20is%20a%20standard%20hard,in%20nearly%20all%20Linux%20distributions.


