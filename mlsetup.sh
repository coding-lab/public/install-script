sudo apt update

wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo apt install ./google-chrome-stable_current_amd64.deb
rm  -rf google-chrome-stable_current_amd64.deb

sudo snap install code --classic

sudo snap install teams

sudo apt install git -y


sudo snap install postman


sudo snap install android-studio --classic

sudo apt install openvpn -y
sudo apt-get install filezilla -y


sudo snap install dbeaver-ce

sudo snap install flutter --classic
 
sudo apt install nodejs -y
sudo apt install npm -y
sudo snap install curl

sudo npm cache clean -f
sudo npm install -g n
sudo n stable

sudo apt-get install openconnect network-manager-openconnect network-manager-openconnect-gnome -y


sudo npm install -g @angular/cli

sudo apt install libpam-aad libnss-aad

sudo pam-auth-update --enable mkhomedir

sudo snap install obsidian --classic 

sudo apt update
sudo apt install snapd -y

sudo snap install zaproxy --classic


sudo apt-get install -y python3 python3-virtualenv python3-pip wkhtmltopdf libpq-dev libxmlsec1-dev pkg-config libxml2-dev libxslt-dev xmlsec1 libpcap-dev libpq-dev
sudo apt install python3.9-venv -y

sudo apt-get install gcc libpq-dev -y
sudo apt-get install python-dev  python-pip -y
sudo apt-get install python3-dev python3-pip python3-venv python3-wheel -y
sudo apt-get install build-essential libssl-dev libffi-dev unixodbc-dev musl-dev g++ -y

pip3 install wheel

pip3 install --upgrade pip setuptools

# VSCode
code --install-extension alefragnani.project-manager
code --install-extension bibhasdn.django-html
code --install-extension Dart-Code.dart-code
code --install-extension Dart-Code.flutter
code --install-extension donjayamanne.githistory
code --install-extension eamodio.gitlens
code --install-extension hediet.vscode-drawio
code --install-extension jcbuisson.vue
code --install-extension MS-CEINTL.vscode-language-pack-it
code --install-extension ms-python.python
code --install-extension ms-python.vscode-pylance
code --install-extension ms-toolsai.jupyter
code --install-extension ms-toolsai.jupyter-keymap
code --install-extension ms-toolsai.jupyter-renderers
code --install-extension ms-vsliveshare.vsliveshare
code --install-extension octref.vetur
code --install-extension puppet.puppet-vscode
code --install-extension redhat.vscode-commons
code --install-extension redhat.vscode-xml
code --install-extension searKing.preview-vscode

# TODO sonarqube
# https://gist.github.com/dmancloud/0abf6ad0cb16e1bce2e907f457c8fce9

# TODO dependency check
#https://jeremylong.github.io/DependencyCheck/dependency-check-cli/ 